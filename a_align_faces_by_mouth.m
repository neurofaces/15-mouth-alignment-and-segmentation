%##########################################################################
%% ZERO STEP: ALIGN FACES
%##########################################################################

clear, clc, close all;

source_imgs_folder = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\';
masks_folder = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\_masks-corrected\';
landmarks_folder = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\_landmarks-corrected\';
results_folder = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\by-centroid\';
% results_folder = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\by-corners-middle-point\';
if(~exist(results_folder, 'dir')), mkdir(results_folder); end;
if(~exist(fullfile(results_folder, 'img'), 'dir')), mkdir(fullfile(results_folder, 'img')); end;
if(~exist(fullfile(results_folder, 'mask'), 'dir')), mkdir(fullfile(results_folder, 'mask')); end;
if(~exist(fullfile(results_folder, 'lm'), 'dir')), mkdir(fullfile(results_folder, 'lm')); end;


% list all 'extension' files in 'folder_imgs'
imgs = dir([source_imgs_folder '*.jpg']);
n_img_files = numel(imgs);

t=tic;
% start the loop for every image in the list
parfor nf=1:n_img_files
    
    disp(['Image ' num2str(nf) '/' num2str(n_img_files)])
    
    % % Image file name
    img_name = strrep(imgs(nf).name, '.jpg', '');
    
    % % Load image
    img = imread(fullfile(source_imgs_folder, [img_name '.jpg']));
    
    % % Load mask
    mask = imread(fullfile(masks_folder, [img_name '_mask_M.bmp']));
    
    % % Load landmarks
    lm = load(fullfile(landmarks_folder, [img_name '.lm']));
    
    % % Align face
    [c_img, c_mask, c_lm] = center_face_by_mouth_centroid(img, mask, lm);
    % [c_img, c_mask, c_lm] = center_face_by_mouth_corners_middle_point(img, mask, lm);
    
    % % Save aligned face, mask and landmarks
    imwrite(c_img, fullfile(results_folder, 'img', [img_name '.bmp']));
    imwrite(c_mask, fullfile(results_folder, 'mask', [img_name '.bmp']));
    csvwrite(fullfile(results_folder, 'lm', [img_name '.lm']), c_lm);
    
end
disp('Alignment execution ended.');
toc(t)