%##########################################################################
%% FIRST STEP: COARSE NOSE EXTRACTION
%##########################################################################

clear, clc, close all;

verbose = false;

method = 'by-centroid';
% method = 'by-corners-middle-point';

source_imgs_folder = ['D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\' method '\img\'];
masks_folder = ['D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\' method '\mask\'];
landmarks_folder = ['D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\' method '\lm\'];
results_folder = ['D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\' method '\coarse\'];
if(~exist(results_folder, 'dir')), mkdir(results_folder); end;

% list all 'extension' files in 'folder_imgs'
imgs = dir([source_imgs_folder '*.bmp']);
n_img_files = numel(imgs);


%% get dimensions of all masks
masks_size = zeros(n_img_files, 4);
for nf=1:n_img_files
    
    disp(['Getting dimensions of image ' num2str(nf) '/' num2str(n_img_files)])
    
    mask_name = strrep(imgs(nf).name, '.jpg', '_mask_N.bmp');
    c_mask = imread(fullfile(masks_folder, mask_name));
    
    props = regionprops(c_mask, 'Area', 'BoundingBox');
    areas = [props.Area];
    masks_size(nf, :) = props(max(areas) == areas).BoundingBox();
    masks_size(nf, 3) = masks_size(nf, 1) + masks_size(nf, 3);
    masks_size(nf, 4) = masks_size(nf, 2) + masks_size(nf, 4);
    
end

% testing
if(verbose)
    f = figure; imshow(c_mask), hold on
    for i=1:size(masks_size,1)
        plot(masks_size(i,1), masks_size(i,2), 'r.') % top left
        plot(masks_size(i,1), masks_size(i,4), 'g.') % top right
        plot(masks_size(i,3), masks_size(i,4), 'b.') % bottom right
        plot(masks_size(i,3), masks_size(i,2), 'w.') % bottom left
    end
end

% get biggest mask
mask_coords = [min(masks_size(:,1)) min(masks_size(:,2)) max(masks_size(:,3)) max(masks_size(:,4))];
mask_size = [round(mask_coords(3)-mask_coords(1)) round(mask_coords(4)-mask_coords(2))];
if(verbose)
    plot(mask_coords(1), mask_coords(2), 'r*') % top left
    plot(mask_coords(1), mask_coords(4), 'g*') % top right
    plot(mask_coords(3), mask_coords(4), 'b*') % bottom right
    plot(mask_coords(3), mask_coords(2), 'w*') % bottom left
end

t=tic;
%% Coarse mouth extraction
mean_mouth = uint16(zeros([fliplr(mask_size) 3]));
v = VideoWriter(['mouths_' method '.bmp'],'MPEG-4');%'Motion JPEG 2000');%'Uncompressed AVI');%'MPEG-4');
% v.CompressionRatio = 5;
v.FrameRate = 5;
open(v);
for nf=1:n_img_files
    
    disp(['Coarse mouth extraction for image ' num2str(nf) '/' num2str(n_img_files)])
    
    % % Image file name
    img_name = strrep(imgs(nf).name, '.bmp', '');
    
    % % Load image
    c_img = imread(fullfile(source_imgs_folder, [img_name '.bmp']));
    [ho, wo, ~] = size(c_img);
    
    % % Load mask
    c_mask = logical(imread(fullfile(masks_folder, [img_name '.bmp'])));
    
    % % Load lm
    c_lms = load(fullfile(landmarks_folder, [img_name '.lm']));
    
    % % Cut the mouth out and correct the landmarks
    mouth_wo_mask = imcrop(c_img, [mask_coords(1:2) mask_size-1]);
    mask = imcrop(c_mask, [mask_coords(1:2) mask_size-1]);
    mouth_w_mask = mouth_wo_mask.*repmat(uint8(mask),1,1,3);
    % keep only mouth landmarks
    % lm.chehra.OutterMouth = 32:43; +17
    % lm.chehra.InnerMouth = 44:49; +17
    m_ilm = (32:49) + 17;
    m_lms(:,1) = c_lms(m_ilm, 1) - repmat(mask_coords(1), numel(c_lms(m_ilm, 1)), 1);
    m_lms(:,2) = c_lms(m_ilm, 2) - repmat(mask_coords(2), numel(c_lms(m_ilm, 1)), 1);
    
    if(verbose)
        figure, imshow(c_img), hold on
        for n=1:numel(c_lms(:,1))
            plot(c_lms(n, 1), c_lms(n, 2), 'g.')
        end

        figure, imshow(mouth_wo_mask), hold on
        for n=1:numel(m_lms(:,1))
            plot(m_lms(n, 1), m_lms(n, 2), 'r.')
        end
    end
    
    % % Create a video with all noses to see diferences among them
    mean_mouth(:,:,1) = mean_mouth(:,:,1) + uint16(mouth_w_mask(:,:,1));
    mean_mouth(:,:,2) = mean_mouth(:,:,2) + uint16(mouth_w_mask(:,:,2));
    mean_mouth(:,:,3) = mean_mouth(:,:,3) + uint16(mouth_w_mask(:,:,3));
    writeVideo(v, mouth_w_mask);
    
    % % Save extracted nose
    if(~exist(fullfile(results_folder, 'mouth_wo_mask'), 'dir')), mkdir(fullfile(results_folder, 'mouth_wo_mask')); end;
    if(~exist(fullfile(results_folder, 'mouth_w_mask'), 'dir')), mkdir(fullfile(results_folder, 'mouth_w_mask')); end;
    if(~exist(fullfile(results_folder, 'mask'), 'dir')), mkdir(fullfile(results_folder, 'mask')); end;
    if(~exist(fullfile(results_folder, 'lm'), 'dir')), mkdir(fullfile(results_folder, 'lm')); end;
    imwrite(mouth_wo_mask, fullfile(results_folder, 'mouth_wo_mask', [img_name '.bmp']));
    imwrite(mouth_w_mask, fullfile(results_folder, 'mouth_w_mask', [img_name '.bmp']));
    imwrite(mask, fullfile(results_folder, 'mask', [img_name '.bmp']));
    csvwrite(fullfile(results_folder, 'lm', [img_name '.lm']), m_lms);
    
end
mean_mouth = uint8(mean_mouth / n_img_files);
imwrite(mean_mouth, ['mean_mouth_' method '.bmp']);
close(v);
disp('Execution ended.');
toc(t)