% c_resize_images
verbose = false;
new_size = 0.5; %0.25;

folder_imgs = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\by-centroid\coarse\mouth_wo_mask\';
folder_masks = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\by-centroid\coarse\mask\';
folder_lms = 'D:\DOCTORADO\01_DATA\_NoGIT_common_dbs\cfd\faces-aligned-by-mouth\by-centroid\coarse\lm\';
folder_results = fullfile(folder_imgs, 'cropped\');
folder_results_masks = fullfile(folder_imgs, 'cropped\masks\');
folder_results_lms = fullfile(folder_imgs, 'cropped\lm\');
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;
if(~exist(folder_results_masks, 'dir')), mkdir(folder_results_masks); end;
if(~exist(folder_results_lms, 'dir')), mkdir(folder_results_lms); end;
extension = '.bmp';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs '*' extension]);
n_img_filenames = numel(img_filenames);

% start the loop for every image in the list to resize images
hw = waitbar(0, 'Resizing images... 0%');
for nf=1:n_img_filenames
    
    waitbar(nf/n_img_filenames, hw, sprintf('Resizing images... %.2f%%', nf/n_img_filenames*100));
    cprintf('blue', ['Resizing image ' num2str(nf) '/' num2str(n_img_filenames) '\n']);
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    mask = imread(fullfile(folder_masks, [filename extension]));
    lm = load(fullfile(folder_lms, [filename '.lm']));
    % % Crop
    % img = imcrop(img, [50 225 310 195]);
    
    % % Resize
	resized_img = imresize(img, new_size);
    resized_mask = imresize(mask, new_size);
    resized_lm = lm * new_size;
    
    if(verbose)
        figure, imshow(resized_img), hold on
        for n=1:numel(resized_lm(:,1))
            plot(resized_lm(n,1), resized_lm(n,2), 'r.');
        end
    end
    
    % % Save image and landmarks
    imwrite(resized_img, fullfile(folder_results, strcat(filename, '.bmp')));
    imwrite(resized_mask, fullfile(folder_results_masks, strcat(filename, '.bmp')));
    csvwrite(fullfile(folder_results_lms, strcat(filename, '.lm')), resized_lm);
    
end
disp('Execution ended.');
close(hw);