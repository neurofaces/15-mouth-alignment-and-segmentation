% This function centers the face in img using the mouth's centroid.
% Inputs:
%   - img: image with the face uncentered
%   - mask: mask uncentered
%   - lm: landmarks uncentered
% Outputs:
%   - c_img: image centered
%   - c_mask: mask centered
%   - c_lm: landmarks centered
function [c_img, c_mask, c_lm] = center_face_by_mouth_corners_middle_point(img, mask, lm)

mask(mask == 255) = 1;

% get mouth shape
mouth_shape = roipoly(zeros(size(mask)),lm([49:60 49],1),lm([49:60 49],2));
mask = bwmorph(mouth_shape, 'thicken', 10);
mouth = rgb2gray(img .* uint8(repmat(mask, 1, 1, 3)));

% mouth's centroid
mouth_centroid = mean(lm([49 55],:)); % mouth corners' middle point
nh = round(mouth_centroid(1));
nw = round(mouth_centroid(2));

% middle of image
[w, h, ~] = size(img);
mh = round(h/2); 
mw = round(w/2);

% check if its working ok
% figure, imshow(mouth)
% hold on, plot(nh, nw, 'r.')
% plot(mh, mw, 'b.');

% shift image
dh = mh - nh;
dw = mw - nw;
c_img = uint8(imshift(img, dw, dh));
c_mask = imshift(mouth, dw, dh);
c_lm(:,1) = lm(:,1) + dh;
c_lm(:,2) = lm(:,2) + dw;

% % check if its working ok
% figure, imshow(c_img), hold on
% for i=1:66
%     plot(c_lm(i,1), c_lm(i,2), 'r.')
% end